defmodule NervesLiveview do
  @moduledoc """
  Documentation for NervesLiveview.
  """

  @doc """
  Hello world.

  ## Examples

      iex> NervesLiveview.hello
      :world

  """
  def hello do
    :world
  end
end
